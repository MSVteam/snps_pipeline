// RAW MAPPING OF TRIMMED DATA ON REFERENCE GENOME WITH BWA
process bwa_map {

    tag "BWA map $sample_id"
    conda 'bioconda::bwa-mem2=2.2.1 bioconda::samtools=1.15.1'
    // container 'quay.io/biocontainers/bwa-mem2:2.2.1--he513fc3_0 quay.io/biocontainers/samtools:1.15.1--h6899075_1'
    publishDir "$params.outdir/Mapped_bam/"
    cpus 4

    input:
        tuple val(sample_id), path(reads)
        val bwa_build_bwt
        val samtools_faidx
        val genome

    output:
      tuple val(sample_id), file("*_BWA.bam")

    script:
    rg="@RG\\tID:${sample_id}\\tPL:ILLUMINA\\tSM:${sample_id}"
    """
    bwa-mem2 mem -t ${task.cpus} -R '${rg}' ${genome} ${reads[0]} ${reads[1]} | samtools view -Sb - > ${sample_id}_BWA.bam
    """
}
