// REMOVING DUPLICATES
process picard_rmdup {

    tag "picard_rmdup on $sample_id"
    conda 'bioconda::picard=2.27.4'
    // container 'quay.io/biocontainers/picard:2.27.4--hdfd78af_0'
    publishDir "$params.outdir/rm_dupl_reads/"

    input:
        tuple val(sample_id), path(reads)

    output:
      tuple val(sample_id), file("*_dupl_reads.bam")

    shell:
    """
    PICARD=`find ${projectDir}/work/conda -type f -name "picard.jar"`
    java -jar -Xmx1g \$PICARD MarkDuplicates I=${reads} O=${sample_id}_dupl_reads.bam VALIDATION_STRINGENCY=SILENT MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 REMOVE_DUPLICATES=TRUE TMP_DIR=$params.outdir/rm_dupl_reads/ M=log_picard.log
    """
}
