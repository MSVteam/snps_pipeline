// VARIANT CALLING STEP 1 : Creating one gvcf file for each sample, and a list of the gvcf files to merge in the following steps.
process picard_dict {

    tag "Creating dictionary for ${genome}"
    conda 'bioconda::picard=2.27.4'
    // container 'quay.io/biocontainers/picard:2.27.4--hdfd78af_0'
    publishDir "$params.datadir"

    input:
      val genome

    output:
      file("*.dict")

    shell:
    def prefix = task.ext.prefix ?: "${genome.baseName}"
    """
    PICARD=`find ${projectDir}/work/conda -type f -name "picard.jar"`
    java -jar \$PICARD CreateSequenceDictionary R=${genome} O=${prefix}.dict
    """
}
