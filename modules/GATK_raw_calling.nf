process GATK_raw_calling {

    tag "Raw GVCF calling on Cleaned bam file for $sample_id"
    conda 'bioconda::gatk4=4.1.7.0 bioconda::samtools=1.15.1'
    // container 'quay.io/biocontainers/gatk4:4.1.7.0--py38_0 quay.io/biocontainers/samtools:1.15.1--h6899075_1'
    publishDir "$params.outdir/Raw_calling"

    input:
      tuple val(sample_id), path(bam)
      val genome
      val dict

    output:
      tuple val(sample_id), file("*.g.vcf*")

    script:
    """
    samtools index -@ ${task.cpus} ${bam}
    gatk --java-options '-Xmx4G' HaplotypeCaller -R ${genome} -ERC GVCF -I ${bam} -O ${sample_id}.g.vcf
    """
}
