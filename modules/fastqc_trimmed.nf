// QUALITY CHECK OF TRIMMED DATA
process fastqc_trimmed {

    tag "FASTQC on trimmed $sample_id"
    conda 'bioconda::fastqc=0.11.9'
    // container 'quay.io/biocontainers/fastqc:0.11.9--hdfd78af_1'
    publishDir "$params.outdir/Cleaned_data/$sample_id"
    cpus 4

    input:
        tuple val(sample_id), path(reads)

    output:
      file("*.{html,zip}")

    script:
    """
    fastqc -t ${task.cpus} ${reads}
    """
}
