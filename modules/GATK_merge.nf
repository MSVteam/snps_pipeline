process GATK_merge {

    tag "Merged_raw_calling_db"
    conda 'bioconda::gatk4=4.1.7.0'
    // container 'quay.io/biocontainers/gatk4:4.1.7.0--py38_0'
    publishDir "$params.outdir/Merged_raw_calling_db/"

    input:
      tuple val(sample_id), path(g_vcf)
      val genome
      val chr_list

    output:
        env x

    script:
    """
    ls -1 $params.outdir/Raw_calling/*g.vcf > $params.outdir/fileList.list
    if [ -d $params.outdir/Merged_raw_calling_db/ ]; then
    echo "Remove $params.outdir/Merged_raw_calling_db/ folder to recreate database"
    rm -fr $params.outdir/Merged_raw_calling_db/
    fi
    gatk --java-options '-Xmx20G' GenomicsDBImport --overwrite-existing-genomicsdb-workspace True -R ${genome} --variant $params.outdir/fileList.list --intervals ${chr_list} --merge-input-intervals --genomicsdb-workspace-path $params.outdir/Merged_raw_calling_db/
    x="GATK_merge process - OK"
    """
}
