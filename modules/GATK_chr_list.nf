// VARIANT CALLING STEP 2 : Merging all the GVCF files.
process GATK_chr_list {

    tag "Creating the sequence list for GenomicsDBImport"
    conda 'bioconda::picard=2.27.4'
    // container 'quay.io/biocontainers/picard:2.27.4--hdfd78af_0'
    publishDir "$params.outdir"

    input:
      val genome
      val dict
      val scriptsdir

    output:
      file("chromosomes.list")

    shell:
    """
    if test -f "${genome}.dict"; then
      echo "${genome}.dict exists remove it to recreate it."
      rm ${genome}.dict
    fi
    PICARD=`find ${projectDir}/work/conda -type f -name "picard.jar"`
    java -jar \$PICARD CreateSequenceDictionary R=${genome} O=${genome}.dict
    ${scriptsdir}/create_chromosome_list.sh ${genome}.dict > chromosomes.list
    """
}
