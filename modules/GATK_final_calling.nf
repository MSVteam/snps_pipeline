// VARIANT CALLING STEP 3 : Converting the resulting gvcf file to a vcf file. End of the variant calling.
process GATK_final_calling {

    tag "Merged_raw_calling_db"
    conda 'bioconda::gatk4=4.1.7.0'
    // container 'quay.io/biocontainers/gatk4:4.1.7.0--py38_0'
    publishDir "$params.outdir/Final_calling/"

    input:
      val vcfheader
      val genome

    output:
        file("*.vcf")

    script:
    """
    gatk --java-options '-Xmx4G' GenotypeGVCFs -R ${genome} -V gendb://"$params.outdir/Merged_raw_calling_db" --genomicsdb-use-vcf-codec -O Variant_calling_merged.vcf
    """
}
