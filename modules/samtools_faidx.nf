// RAW MAPPING OF TRIMMED DATA ON REFERENCE GENOME WITH BWA : creation of SAMTOOLS index file
process samtools_faidx {

    tag "samtools faidx on ${genome}"
    conda 'bioconda::samtools=1.15.1'
    // container 'quay.io/biocontainers/samtools:1.15.1--h6899075_1'
    publishDir "$params.datadir"

    input:
        val genome

    output:
      file("*.fai")

    script:
    def prefix = task.ext.prefix ?: "${genome.baseName}"
    """
    samtools faidx --fai-idx ${prefix}.fasta.fai ${genome}
    """
}
