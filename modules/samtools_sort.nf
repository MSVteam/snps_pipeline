// SORTING READS
process samtools_sort {

    tag "samtools sort on $sample_id"
    conda 'bioconda::samtools=1.15.1'
    // container 'quay.io/biocontainers/samtools:1.15.1--h6899075_1'
    publishDir "$params.outdir/Sorted_bam/"
    cpus 4

    input:
        tuple val(sample_id), path(reads)

    output:
      tuple val(sample_id), file("*_BWA_samtools_sorted.bam")

    script:
    """
    samtools sort -@ ${task.cpus} ${reads} -o ${sample_id}_BWA_samtools_sorted.bam
    """
}
