// Remove all bam files and g.vcf files from work and all the symbolic links in "results" folder
// In order to keep only the final merged variant calling from GATK_final_calling process
process remove_data {

    tag "Remove all trimmed fastq files and bam files"

    input:
      val vcf

    output:
        env x, optional: true

    script:
    """
    rm -fr $params.outdir/Mapped_bam/ $params.outdir/Sorted_bam/ $params.outdir/rm_dupl_reads/ $params.outdir/Cleaned_alignments $params.outdir/Cleaned_data/*fastq* $params.outdir/Cleaned_data/*fq* # $params.outdir/Raw_calling
    find ${projectDir}/work/ -type f -name "*.bam" | xargs rm
    #find ${projectDir}/work/ -type f -name "*.g.vcf" | xargs rm
    x="remove_data process - OK"
    """
}
