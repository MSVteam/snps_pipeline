// CLEANING READS : Keeping only primary alignment, properly paired and unique.
process clean_alignment {

    tag "Cleaning alignments with Samtools for $sample_id"
    conda 'bioconda::samtools=1.15.1'
    // container 'quay.io/biocontainers/samtools:1.15.1--h6899075_1'
    publishDir "$params.outdir/Cleaned_alignments"
    cpus 4

    input:
      tuple val(sample_id), path(reads)

    output:
      tuple val(sample_id), file("*_cleaned.bam")

    script:
    """
    samtools view -@ ${task.cpus} -b -F 0x100 -f 0x02 -q 1 ${reads} > ${sample_id}_cleaned.bam
    """
}
