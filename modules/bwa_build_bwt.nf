// RAW MAPPING OF TRIMMED DATA ON REFERENCE GENOME WITH BWA : creation of BWA index files
process bwa_build_bwt {

    tag "bwa-mem2 indexing on ${genome}"
    conda 'bioconda::bwa-mem2=2.2.1'
    // container 'quay.io/biocontainers/bwa-mem2:2.2.1--he513fc3_0'
    publishDir "$params.datadir"

    input:
        val genome

    output:
      file("*.0123")
      file("*.bwt.2bit.64")
      file("*.amb")
      file("*.ann")
      file("*.pac")

    script:
    def prefix = task.ext.prefix ?: "${genome.baseName}"
    """
    bwa-mem2 index -p ${prefix}.fasta ${genome}
    """
}
