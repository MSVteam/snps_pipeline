# SNPs_pipeline



## Getting started

Example of command to launch the pipeline : 

`nextflow run pipeline_SNPs.nf --reads /path/to/snps_pipeline/data/*_{1,2}.fastq --outdir /path/to/snps_pipeline/results --datadir /path/to/snps_pipeline/data --scriptsdir /path/to/snps_pipeline/scripts --genome /path/to/snps_pipeline/data/genome.fasta`

with datadir, the folder containing the genome.fasta file.

## Authors
Sophie Blanc
Amandine Velt
