#!/usr/bin/env bash
module load nextflow/23.10.1.5891

# create sample data
module load seqtk/1.3
seqtk sample -s 123 data/ERR8014965_1.fastq 1000000 > data/ERR8014965_1_subsample.fastq
seqtk sample -s 123 data/ERR8014965_2.fastq 1000000 > data/ERR8014965_2_subsample.fastq
seqtk sample -s 123  data/ERR8014964_1.fastq 1000000 > data/ERR8014964_1_subsample.fastq
seqtk sample -s 123  data/ERR8014964_2.fastq 1000000 > data/ERR8014964_2_subsample.fastq

# voir https://stackoverflow.com/questions/77953497/nextflow-only-processes-one-of-my-paired-end-samples
# voir : https://community.seqera.io/t/nextflow-only-processes-one-of-my-paired-end-samples/460
# voir : https://github.com/Grapedia/Tailoring-grapedia-training-school/blob/main/day_2/workflows/AssemblyONT/modules/polish-task.nf
nextflow run pipeline_SNPs.nf -resume
