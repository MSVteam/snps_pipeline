#!/usr/bin/env bash
grep '@SQ' $1 | cut -f2,3 | sed 's/SN:\([^\\t]*\)\\tLN:\([0-9]*\)/\\1:1-\\2/' | sed "s/\t.*//" | sed "s/SN://"
