// Path to fastq files
params.reads="${projectDir}/data/*_{1,2}_subsample.fastq"
// Path to outdir
params.outdir = "${projectDir}/results"
// Path to reference genome
params.genome = "${projectDir}/data/PN40024_40X_REF_chloro_mito.chr_renamed.fasta"

// create a tuple contaning sampleID and path to paired-end fastq files
// example : [ERR8014965, [/data2/avelt/2024_pipeline_SNPs_nextflow/work/db/22bc6a702cbf0d92ade8fd31b0916b/ERR8014965_1.trimmed_Q20.fastq.gz, /data2/avelt/2024_pipeline_SNPs_nextflow/work/db/22bc6a702cbf0d92ade8fd31b0916b/ERR8014965_2.trimmed_Q20.fastq.gz]]
reads_ch = channel.fromFilePairs( params.reads, checkIfExists: true )

// genome channel
genome_channel = Channel.fromPath(params.genome, checkIfExists: true )

// we have to convert genome channel in value in order to be able to use it multiple times in bwa_map
Channel
  .fromPath(params.genome,
            checkIfExists: true)
  .first()
  .set { genome_ch }

// Path to datadir (with fastq files, genome ...)
params.datadir = "${projectDir}/data"
// Path to scriptdir with sh script
params.scriptsdir = "${projectDir}/scripts"

include { fastqc_raw } from "./modules/fastqc_raw"
include { trim_raw } from "./modules/trim_raw"
include { fastqc_trimmed } from "./modules/fastqc_trimmed"
include { bwa_build_bwt } from "./modules/bwa_build_bwt"
include { samtools_faidx } from "./modules/samtools_faidx"
include { bwa_map } from "./modules/bwa_map"
include { samtools_sort } from "./modules/samtools_sort"
include { picard_rmdup } from "./modules/picard_rmdup"
include { clean_alignment } from "./modules/clean_alignment"
include { picard_dict } from "./modules/picard_dict"
include { GATK_raw_calling } from "./modules/GATK_raw_calling"
include { GATK_chr_list } from "./modules/GATK_chr_list"
include { GATK_merge } from "./modules/GATK_merge"
include { GATK_final_calling } from "./modules/GATK_final_calling"
include { remove_data } from "./modules/remove_data"

workflow {
    fastqc_raw(reads_ch)
    trim_raw(reads_ch)
    fastqc_trimmed(trim_raw.out)
    bwa_build_bwt(genome_ch)
    samtools_faidx(genome_ch)
    bwa_map(trim_raw.out, bwa_build_bwt.out[0], samtools_faidx.out, genome_ch)
    samtools_sort(bwa_map.out)
    picard_rmdup(samtools_sort.out)
    clean_alignment(picard_rmdup.out)
    picard_dict(genome_ch)
    // we use collect in order to wait that all processes are finished (all VCFs files are generated) before launching GATK_merge process
    GATK_raw_calling_channel=GATK_raw_calling(clean_alignment.out, genome_ch, picard_dict.out) | collect
    GATK_chr_list(genome_ch, picard_dict.out, params.scriptsdir)
    GATK_merge(GATK_raw_calling_channel, genome_channel, GATK_chr_list.out)
    GATK_final_calling(GATK_merge.out, genome_ch)
    remove_data(GATK_final_calling.out)
}
